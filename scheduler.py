import os 
import shutil
import datetime
from subprocess import call
from apscheduler.schedulers.blocking import BlockingScheduler


# get files
LSF = ['C:\\Users\\Administrator\\Desktop\\LSF\\RPA-Python\\exLSFextractLoad.py', 
    'C:\\Users\\Administrator\\Desktop\\LSF\\RPA-Python\\exLSFfire.py', 
    'C:\\Users\\Administrator\\Desktop\\LSF\\RPA-Python\\exLSFhandlecsv.py', 
    'C:\\Users\\Administrator\\Desktop\\LSF\\RPA-Python\\exLSFreport.py']

dir = os.path.dirname(os.path.realpath(__file__))

sched = BlockingScheduler()

def tick(txt):
    print('[INFO][{event}]: {time}'.format(event=txt,time=datetime.datetime.now().strftime("%d/%m/%Y ,%H:%M:%S")))

def print_running():
    print("[INFO] : APScheduler is running {time}".format(time=datetime.datetime.now().strftime("%d/%m/%Y ,%H:%M:%S")))

def run_rpa():
    tick('run RPA')
    call(["python", LSF[3]])

def move_csv():
    tick('move CSV')
    call(["python", LSF[2]])

def extract_csv():
    tick('extract csv')
    call(["python", LSF[0]])  

def firemail():
    tick('fire mail')
    call(["python", LSF[1]])           

# print status every 6 hours
sched.add_job(print_running,'interval', hours=6)

# export LSF report at 6 AM and 1 PM
sched.add_job(run_rpa,'cron', day_of_week='mon-fri', hour=6)
sched.add_job(run_rpa,'cron', day_of_week='mon-fri', hour=13)

# move LSF report at 6 AM 15 mins and 1 PM 15 mins
sched.add_job(move_csv,'cron', day_of_week='mon-fri', hour=6, minute=15)
sched.add_job(move_csv,'cron', day_of_week='mon-fri', hour=13, minute=15)

# extract LSF report at 6 AM 15 mins and 1 PM 15 mins
sched.add_job(extract_csv,'cron', day_of_week='mon-fri', hour=6, minute=30)
sched.add_job(extract_csv,'cron', day_of_week='mon-fri', hour=13, minute=30)

# extract LSF report at 6 AM 15 mins and 1 PM 15 mins
sched.add_job(firemail,'cron', day_of_week='mon-fri', hour=7)
sched.add_job(firemail,'cron', day_of_week='mon-fri', hour=14)

try:
    print('[INFO] : start scheduler')
    sched.start()
except (KeyboardInterrupt):
    print('Got SIGTERM! Terminating...')