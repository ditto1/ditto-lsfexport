# RPA export report manually from Laserfiche report
# timeout = 10s
import rpa as r

def clickLoadReport():
    r.click('a[title=\"Reports\"]')
    r.wait(3) # routing
    if r.exist("//*[text()='Custom Reports']"):
        r.click("//*[text()='Custom Reports']")
        r.wait(3) # routing
        r.type('#report-searchBar','PV monitor - Brief')
        r.wait(5)
        if r.exist('a[title=\"PV monitor - Brief\"]'):
            r.click('a[title=\"PV monitor - Brief\"]')
            r.wait(5)
            r.click('button[title=\"Download\"]')
            if r.exist('div[title=\"XLSX\"]'):
                r.click('div[title=\"XLSX\"]')
                if r.exist("//*[text()='CSV']"):
                    r.click("//*[text()='CSV']")
                    r.click("//*[text()='Download']")
                    r.wait(10) #loading

r.init()
r.url('http://localhost/Forms')
# For web automation, the web element identifier can be XPath selector,
# CSS selector, or the following attributes - id, name, class, title, aria-label, text(), href, 
# in decreasing order of priority. Recommend writing XPath manually or simply using attributes
# sign in if not sign in 
if r.exist("//*[text()='Sign In']"):
    r.type('#UserName', 'sadmin')
    r.type('#Password', 'Ditto12345$')
    r.click('submit')

# loading 
# login success
if r.exist('a[title=\"Reports\"]'):
    clickLoadReport()
    r.close()
# unable to login because of time out    
else:
    print('error')   


# if __name__ == '__main__':
#     loop = asyncio.get_event_loop()
#     loop.run_until_complete(run())
#     loop.close()    
