import smtplib
import json
import os 
import csv
import datetime
from os import listdir
from os.path import isfile, join
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.utils import make_msgid
import time

smtp_server = "mail.dittothailand.com"
port = 587  
sender_email = "admin@dittothailand.com"
password = "Ditto12345$"
now = datetime.datetime.now()

message = MIMEMultipart("alternative")
subject = "[LSF] {no} New Forms Notification " + now.strftime("%d/%m/%Y ,%H:%M:%S")
message["From"] = sender_email


context = """\
<html>
  <body>
    <p>-- AUTO - MESSAGE, DO NOT REPLY--<br>
    <br>
       Dear recipant,<br>
       <br>
       You/Your team have new {no} {task} tasks waited for your action.<br>
       <br>
       please sign in to Laserfiche Forms to proceed.<br>
       <br>
       <a href="http://161.82.233.60/Forms">http://161.82.233.60/Forms</a> <br>
       <br>
       Best Regards<br>
       <br>
       -- AUTO - MESSAGE, DO NOT REPLY--<br />
    </p>
  </body>
</html>
"""

def firemail(recipients,header,count):
    try:
        server = smtplib.SMTP(smtp_server,port)
        server.ehlo() # Can be omitted
        server.login(sender_email, password)

        message["Subject"] = subject.format(no=count)
        message['Message-ID'] = make_msgid()
        if len(recipients)>1:
            message["To"] = ", ".join(recipients)
        else:
            message["To"] = recipients[0]
        html = context.format(no=count,task=header)

        #Send email here
        msgbody = MIMEText(html, "html")

        message.attach(msgbody)

        server.sendmail(
            sender_email, recipients, message.as_string()
        )

    except Exception as e:
        # Print any error messages to stdout
        print(e)
    finally:
        print('send to '+ message["To"] )
        time.sleep(5)
        print('resent another in 5 secs')
        del message["Subject"], message["To"], message['Message-ID']
        server.quit() 

dir = os.path.dirname(os.path.realpath(__file__))
load_path = dir+'\\csv\\_load\\'
load_files = [f for f in listdir(load_path) if isfile(join(load_path, f))]

# get mail list
with open(dir+'\\'+'mailTeamPair.json') as f:
  maillist = json.load(f)

# open and ready mail server

# get all paths in loads
# for each load file
with open(load_path+load_files[0], mode='r',encoding='UTF-8') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for row in csv_reader:
        if row[0]:
            # omitted header
            # row : 0,CHAITHAD,PV1,การเงิน,1
            if row[1] == 'Manager':
                firemail(maillist[row[1]][row[3]],row[2],row[4])
            else:
                # firemail recipant head count
                firemail(maillist[row[1]],row[2],row[4])