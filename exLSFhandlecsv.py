import os 
import shutil
from os import listdir
from os.path import isfile, join

# window path
pv_path = 'csv\\PV\\'

# get csv
dir = os.path.dirname(os.path.realpath(__file__))
files = [f for f in listdir(dir) if isfile(join(dir, f))]
PVcsv = list(filter(lambda f : "PV_monitor_-_Brief" in f, files))
# move csv into path
for r in PVcsv:
    shutil.move(dir +'\\'+ r, dir +'\\'+ pv_path + r)
    print('moved ' + r +' to '+dir +'\\'+ pv_path + r)