import os 
import shutil
import pandas as pd
import glob
import math
from os import listdir
from os.path import isfile, join
list_csv = []; df = None

# get all folders
dir = os.path.dirname(os.path.realpath(__file__))
csv_path = dir+'\\csv\\'
exception = ['_load']
folders = [f for f in listdir(csv_path) if f not in exception]

# get latest export from each folder
for fol in folders:
    list_of_files = glob.glob(csv_path+fol+'\\'+'*.csv') # * means all if need specific format then *.csv
    latest_file = max(list_of_files, key=os.path.getctime)
    list_csv.append(latest_file)

# return memories
del exception, folders, dir

# import and concat in pandas
for csv in list_csv:
    df = pd.read_csv(csv)

    # clean head
    df['header'] = df['รายการ'].map(lambda x:x.split('|')[0] if (not pd.isna(x)) else 'None')
    dfg = df.groupby(['assignedto','header','dept'])['รายการ'].count().reset_index()

    # get group df
    f_name = csv.split('\\')[-1]
    dfg.to_csv(csv_path+'_load\\_fire_'+f_name) 

del list_csv, df,f_name    